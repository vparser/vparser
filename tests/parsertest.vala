/* parsertest.vala
 *
 * Copyright (C) 2011  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 *    Tomaž Vajngerl <quikee@gmail.com>
 */

using Parsing;

void test_language_3 () {
	var language3 = new Language3 ();
	var source = new SourceFile.from_file ("language3.lang");
	Parser parser = new Parser (language3, source);
	bool result = parser.parse ();

	if (result == false) {
		parser.print_syntax_errors ();
	}

	assert (result);
}

void test_language_2 () {
	var language2 = new Language2 ();
	var source = new SourceFile.from_file ("language2.lang");
	Parser parser = new Parser (language2, source);
	bool result = parser.parse ();

	if (result == false) {
		parser.print_syntax_errors ();
	}

	assert (result);

	var tree = parser.element.subtree;
	assert (tree.size == 2);

	var subtree = tree[0].subtree;

	assert_tree_list (subtree, {"class", "Person", "{", null, "}"});
	subtree = subtree[3].subtree;
	assert (subtree.size == 3);
	assert_tree_list (subtree[0].subtree, {"int", "id", ";"});
	assert_tree_list (subtree[1].subtree, {"string", "name", ";"});
	assert_tree_list (subtree[2].subtree, {"string", "surname", ";"});

	subtree = tree[1].subtree;
	assert_tree_list (subtree, {"class", "Address", "{", null, "}"});
	subtree = subtree[3].subtree;
	assert (subtree.size == 3);
	assert_tree_list (subtree[0].subtree, {"int", "id", ";"});
	assert_tree_list (subtree[1].subtree, {"string", "city", ";"});
	assert_tree_list (subtree[2].subtree, {"string", "postalCode", ";"});

}

void test_language_1 () {
	var language1 = new Language1 ();
	var source = new SourceFile.from_file ("language1.lang");

	Parser parser = new Parser (language1, source);
	assert (parser.parse ());

	var tree = parser.element.subtree;
	assert (tree.size == 2);

	var subtree = tree[0].subtree;
	assert_tree_list (subtree, {"Root", ":", null, ";"});
	var subsubtree = subtree[2].subtree;
	assert (subsubtree.size == 3);
	assert (subsubtree[0].subtree[0].content == "root content 1");
	assert (subsubtree[1].subtree[0].content == "root content 2");
	assert (subsubtree[2].subtree[0].content == "root content 3");

	subtree = tree[1].subtree;
	assert_tree_list (subtree, {"Element", ":", null, ";"});
	subsubtree = subtree[2].subtree;
	assert (subsubtree.size == 1);
	assert (subsubtree[0].subtree[0].content == "element content");
}

void test_hello_world () {
	var grammar = new And ({new ID (), new Token (","), new ID (), new Token ("!")});
	var parser = new Parser (grammar);
	parser.parse_string ("Hello, World!");
}

void test_nesting () {
	var open = new Token ("{");
	var content = new Forward ();
	var or = new Or ({content, Base.STRING ()});
	var close = new Token ("}");
	var and = new And ({open, or, close});
	content.forward_to = and;

	Parser parser = new Parser (and);
	assert (parser.parse_string ("{{{\"abc\"}}}") == true);

	var subtree = parser.element.subtree;
	assert (subtree.size == 3);
	assert_tree_list (subtree, {"{", null, "}"});

	subtree = subtree[1].subtree;
	assert (subtree.size == 3);
	assert_tree_list (subtree, {"{", null, "}"});

	subtree = subtree[1].subtree;
	assert (subtree.size == 3);
	assert_tree_list (subtree, {"{", "abc", "}"});

	assert (parser.parse_string ("{{{\"abc\"{}}") == false);
	assert_syntax_error (parser, {1,9});

	assert (parser.parse_string ("{{{\"abc\"}}{") == false);
	assert_syntax_error (parser, {1,11});

	assert (parser.parse_string ("}{{\"abc\"}}}") == false);
	assert_syntax_error (parser, {1,1});

	assert (parser.parse_string ("{}{\"abc\"}}}") == false);
	assert_syntax_error (parser, {1,2});

	assert (parser.parse_string ("{{{}}}") == false);
	assert_syntax_error (parser, {1,4});
}

void test_one_or_more_literal () {
	Literal name = new ID ();
	var one_or_more = new OneOrMore (name);
	Parser parser = new Parser (one_or_more);

	assert (parser.parse_string ("Abc Bce Cef Efg") == true);
	assert (parser.element.subtree.size == 4);
	assert (parser.element.subtree[0].content == "Abc");
	assert (parser.element.subtree[1].content == "Bce");
	assert (parser.element.subtree[2].content == "Cef");
	assert (parser.element.subtree[3].content == "Efg");

	assert (parser.parse_string ("Abc") == true);
	assert (parser.parse_string ("") == false);
	assert_syntax_error (parser, {1,1});
	assert (parser.parse_string ("'Abc'") == false);
	assert_syntax_error (parser, {1,1});
	assert (parser.parse_string ("Abc 'Abc'") == false);
	assert_syntax_error (parser, {1,5});

	assert (evaluate_string (one_or_more, "Abc 'Abc'") == true);
}

void test_zero_or_more_literal () {
	Literal name = new ID ();
	var zero_or_more = new ZeroOrMore (name);
	Parser parser = new Parser (zero_or_more);

	assert (parser.parse_string ("Abc Bce Cef Efg") == true);
	assert (parser.element.subtree.size == 4);
	assert (parser.element.subtree[0].content == "Abc");
	assert (parser.element.subtree[1].content == "Bce");
	assert (parser.element.subtree[2].content == "Cef");
	assert (parser.element.subtree[3].content == "Efg");

	assert (parser.parse_string ("Abc") == true);
	assert (parser.element.subtree.size == 1);
	assert (parser.element.subtree[0].content == "Abc");

	assert (parser.parse_string ("") == true);
	assert (parser.element.subtree.is_empty);

	assert (evaluate_string (zero_or_more, "'ABC'") == true);
	assert (parser.parse_string ("'ABC'") == false);
	assert_syntax_error (parser, {1,1});
}

void test_and_literal () {
	Parser parser;

	Literal literal_1 = new Token ("ABC");
	Literal literal_2 = new Token ("DEF");
	var and = new And ({literal_1, literal_2});

	parser = new Parser (and);
	assert (parser.parse_string ("ABC DEF") == true);

	parser = new Parser (and);
	assert (parser.parse_string ("ABC  \t \n \t  DEF") == true);

	literal_1 = new Token ("ABC");
	literal_2 = Base.STRING ();
	and = new And ({literal_1, literal_2});

	parser = new Parser (and);
	assert (parser.parse_string ("ABC  \t \n \t  \"DEF\"     ") == true);

	parser = new Parser (and);
	assert (parser.parse_string ("ABC\"DEF\"") == true);

	parser = new Parser (and);
	assert (parser.parse_string ("ABC 'DEF'") == false);
	assert_syntax_error (parser, {1,5});
}

void test_or_literal () {
	Literal literal_1;
	Literal literal_2;
	Literal literal_3;
	Or or;
	Parser parser;

	literal_1 = new Token ("ABC");
	literal_2 = new Token ("DEF");
	or = new Or ({literal_1, literal_2});
	parser = new Parser (or);

	assert (parser.parse_string ("ABC") == true);
	assert (parser.parse_string ("DEF") == true);
	assert (parser.parse_string ("ABCxxx") == false);
	assert_syntax_error (parser, {1,4});
	assert (parser.parse_string ("DEFxxx") == false);
	assert_syntax_error (parser, {1,4});
	assert (evaluate_string (or, "ABCxxx") == true);
	assert (evaluate_string (or, "DEFxxx") == true);
	assert (parser.parse_string ("abcABC") == false);
	assert_syntax_error (parser, {1,1});
	assert (parser.parse_string ("abcDEF") == false);
	assert_syntax_error (parser, {1,1});
	assert (parser.parse_string ("abc") == false);
	assert_syntax_error (parser, {1,1});

	literal_1 = Base.SSTRING ();
	literal_2 = new ID ();
	literal_3 = Base.STRING ();
	or = new Or ({literal_1, literal_2, literal_3});
	parser = new Parser (or);

	assert (parser.parse_string ("abc") == true);
	assert (parser.parse_string ("XXX") == true);
	assert (parser.parse_string ("\"123\"") == true);
	assert (parser.parse_string ("'123'") == true);
	assert (parser.parse_string ("123") == false);
	assert_syntax_error (parser, {1,1});
}

void test_optional_literal() {
	Parser parser;

	var key = Base.SSTRING ();
	var separator = new Token ("=");
	var value = Base.SSTRING ();

	var optional_and = new And ({key, separator, value});
	var optional = new Optional (optional_and);

	var id = new ID ();
	var semi = new Token (";");
	var and = new And ({id, optional, semi});

	parser = new Parser (and);

	parser.source = new SourceFile.from_string ("identificator1 'a'='1';");
	assert (parser.parse () == true);
	assert (parser.element.subtree.size == 3);
	assert (parser.element.subtree[0].content == "identificator1");
	assert (parser.element.subtree[1].subtree.size == 3);
	assert (parser.element.subtree[2].content == ";");

	parser.source = new SourceFile.from_string ("identificator1;");
	assert (parser.parse () == true);
	assert (parser.element.subtree.size == 3);
	assert (parser.element.subtree[0].content == "identificator1");
	assert (parser.element.subtree[1].subtree.size == 0);
	assert (parser.element.subtree[2].content == ";");

}

void test_token_literal () {
	Token token_literal;
	Parser parser;

	//Check success
	token_literal = new Token ("ABCDEFGHIJKLMNOPRSTUVZ");
	parser = new Parser (token_literal);
	assert (parser.parse_string ("ABCDEFGHIJKLMNOPRSTUVZ") == true);
	assert (parser.element.content == "ABCDEFGHIJKLMNOPRSTUVZ");

	// Check syntax error
	token_literal = new Token ("ABCDEFGHIJKLMNOPRSTUVZ");
	parser = new Parser (token_literal);
	assert (parser.parse_string ("ABCDEF HIJKLMNOPRSTUVZ") == false);
	assert_syntax_error (parser, {1,7});

	// Check syntax error at last character
	token_literal = new Token ("ABCD");
	parser = new Parser (token_literal);
	assert (parser.parse_string ("ABCd") == false);
	assert_syntax_error (parser, {1,4});

	// Check syntax error at first character
	token_literal = new Token ("ABCD");
	parser = new Parser (token_literal);
	assert (parser.parse_string ("aBCD") == false);
	assert_syntax_error (parser, {1,1});
}

void test_string_literal () {
	var string_literal = Base.STRING ();
	Parser parser = new Parser (string_literal);

	parser.source = new SourceFile.from_string ("\"Typical String\"");
	assert (parser.parse () == true);
	assert (parser.element.content == "Typical String");

	parser.source = new SourceFile.from_string ("Invalid Begin");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,1});

	parser.source = new SourceFile.from_string ("\"Sudden end");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,12});

	parser.source = new SourceFile.from_string ("\"Sudden new line\n\"");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,17});
}

void test_sstring_literal () {
	var string_literal = Base.SSTRING ();
	Parser parser = new Parser (string_literal);

	parser.source = new SourceFile.from_string ("'Typical String'");
	assert (parser.parse () == true);
	assert (parser.element.content == "Typical String");

	parser.source = new SourceFile.from_string ("Invalid Begin");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,1});

	parser.source = new SourceFile.from_string ("'Sudden end");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,12});

	parser.source = new SourceFile.from_string ("'Sudden new line\n'");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,17});
}

void test_id_literal() {
	var id = new ID ();
	Parser parser = new Parser (id);

	parser.source = new SourceFile.from_string ("abcd");
	assert (parser.parse () == true);
	assert (parser.element.content == "abcd");

	parser.source = new SourceFile.from_string ("abcd   ddd");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,8});

}

void test_alphanumeric_literal() {
	AlphaNumberic alphaNumberic = new AlphaNumberic ();

	Parser parser = new Parser (alphaNumberic);

	parser.source = new SourceFile.from_string ("abc");
	assert (parser.parse () == true);
	assert (parser.element.content == "abc");

	parser.source = new SourceFile.from_string ("abc  ");
	assert (parser.parse () == true);
	assert (parser.element.content == "abc");

	parser.source = new SourceFile.from_string ("abc1");
	assert (parser.parse () == true);
	assert (parser.element.content == "abc1");

	parser.source = new SourceFile.from_string ("abc!");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});
}

void test_alpha_literal() {
	Alpha alpha = new Alpha ();

	Parser parser = new Parser (alpha);

	parser.source = new SourceFile.from_string ("abc");
	assert (parser.parse () == true);
	assert (parser.element.content == "abc");

	parser.source = new SourceFile.from_string ("abc  ");
	assert (parser.parse () == true);
	assert (parser.element.content == "abc");

	parser.source = new SourceFile.from_string ("abc1");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});

	parser.source = new SourceFile.from_string ("abc!");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});
}

void test_numeric_literal() {
	Numeric numeric = new Numeric ();

	Parser parser = new Parser (numeric);

	parser.source = new SourceFile.from_string ("112");
	assert (parser.parse () == true);
	assert (parser.element.content == "112");

	parser.source = new SourceFile.from_string ("112  ");
	assert (parser.parse () == true);
	assert (parser.element.content == "112");

	parser.source = new SourceFile.from_string ("112a");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});

	parser.source = new SourceFile.from_string ("112.0");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});
}

void test_word_literal() {
	Word word = new Word ((index, character) => {
		return character.isdigit ();
	});

	Parser parser = new Parser (word);

	parser.source = new SourceFile.from_string ("112");
	assert (parser.parse () == true);
	assert (parser.element.content == "112");

	parser.source = new SourceFile.from_string ("112  ");
	assert (parser.parse () == true);
	assert (parser.element.content == "112");

	parser.source = new SourceFile.from_string ("112a");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});

	parser.source = new SourceFile.from_string ("112.0");
	assert (parser.parse () == false);
	assert_syntax_error (parser, {1,4});
}

public bool evaluate_string (Literal literal, string parse_string) {
	var context = new ParserContext (parse_string);
	return literal.evaluate (context);
}

public void assert_syntax_error (Parser parser, int[] location) {
	var syntax_errors = parser.get_list_of_syntax_errors ();
	var line_number = syntax_errors[0].parse_position.line_number;
	var line_character_number = syntax_errors[0].parse_position.line_character_number;

	if (line_number != location[0]) {
		print ("Location line number: %d expected %d\n", line_number, location[0]);
		assert (line_number == location[0]);
	}

	if (line_character_number != location[1]) {
		print ("Location line number: %d expected %d\n", line_character_number, location[1]);
		assert (line_character_number == location[1]);
	}
}

public void assert_tree_list (Gee.List<TreeElement> tree, string[] expected) {
	int index = 0;
	foreach (TreeElement element in tree) {
		assert (element.content == expected[index]);
		index++;
	}
}

public int main (string[] args) {
	Test.init(ref args);

	Test.add_func ("/basic/word literal", test_word_literal);
	Test.add_func ("/basic/numeric literal", test_numeric_literal);
	Test.add_func ("/basic/alpha literal", test_alpha_literal);
	Test.add_func ("/basic/alphanumeric literal", test_alphanumeric_literal);
	Test.add_func ("/basic/id literal", test_id_literal);
	Test.add_func ("/basic/sstring literal", test_sstring_literal);
	Test.add_func ("/basic/string literal", test_string_literal);
	Test.add_func ("/basic/token literal", test_token_literal);
	Test.add_func ("/control/and literal", test_and_literal);
	Test.add_func ("/control/or literal", test_or_literal);
	Test.add_func ("/control/optional literal", test_optional_literal);
	Test.add_func ("/control/one or more literal", test_one_or_more_literal);
	Test.add_func ("/control/zero or more literal", test_zero_or_more_literal);
	Test.add_func ("/control/forward and nesting", test_nesting);

	Test.add_func ("/language/test_hello_world", test_hello_world);
	Test.add_func ("/language/test_language_1", test_language_1);
	Test.add_func ("/language/test_language_2", test_language_2);
	Test.add_func ("/language/test_language_3", test_language_3);

	Test.run ();

	return 0;
}
