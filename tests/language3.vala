using Parsing;

class Language3 : OneOrMore {
	public Language3 () {
		var hello = new Alpha ();
		var comma = new Token (",");
		var world = new Alpha ();
		var exclamation = new Token ("!");

		var and = new And ({hello, comma, world, exclamation});

		base (and);
	}
}
