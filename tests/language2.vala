using Parsing;

class Language2 : OneOrMore {
	public Language2 () {
		var class_token = new Token ("class");
		var class_name = new ID ();
		var open_paranthesis = new Token ("{");
		var close_paranthesis = new Token ("}");

		var string_token = new Token ("string");
		var integer_token = new Token ("int");
		var float_token = new Token ("float");

		var type = new Or ({string_token, integer_token, float_token});

		var property_name = new ID ();

		var class_definition = new And ({type, property_name, Base.semicolon ()});
		var class_content = new OneOrMore (class_definition);

		var and = new And ({class_token, class_name, open_paranthesis, class_content, close_paranthesis});
		base (and);
	}
}
