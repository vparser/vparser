using Parsing;

class Language1 : OneOrMore {
	public Language1 () {
		var name = new ID ();
		var colon = new Token (":");
		var content = new OneOrMore (new And ({Base.STRING ()}));
		var semicolon = new Token (";");
		var and = new And ({name, colon, content, semicolon});
		base (and);
	}
}
