valac \
	--pkg gee-1.0  \
	--pkg libgvc \
	--vapidir=./ \
	parser.vala \
	literal.vala \
	literal-basic.vala \
	literal-control.vala \
	parser-context.vala \
	language1.vala \
	language2.vala \
	graphviz-test.vala \
	-o graphviz-test && ./graphviz-test && xdot test.xdot
