using Parsing;
using Gvc;

public class GraphGenerator : Object {

	Graph graph;
	Gvc.Node? parent_node = null;

	public GraphGenerator (string graph_name) {
		graph = new Graph (graph_name, GraphKind.AGDIGRAPH);
	}

	public Graph generate (TreeElement element) {
		visit_tree (element);
		return (owned)graph;
	}

	string escape (string input) {
		return input.replace ("{", "\\{").replace ("}", "\\}").replace("\\", "\\\\").replace("<", "\\<").replace(">", "\\>");
	}

	Gvc.Node create_node (void* object, string? name) {
		var node_name = @"node$((long)object)";
		var node = graph.find_node (node_name);
		if (node != null) {
			return node;
		}
		var tree_element = object as TreeElement;

		node = graph.create_node (node_name);
		if (tree_element.content == null) {
			node.safe_set ("shape", "Mrecord", "");
			node.safe_set ("color", "gray", "");
		} else {
			node.safe_set ("shape", "record", "");
			node.safe_set ("color", "black", "");
		}

		node.safe_set ("label", @"{ $(escape (name)) | {k1 = v1} | {k2 | v2}}", "");

		return (owned)node;
	}

	public void visit_tree (Parsing.TreeElement element) {
		string label = element.content ?? element.type;

		Gvc.Node node = create_node (element, label);
		if (parent_node != null) {
			var edge = graph.find_edge (parent_node, node);
			if (edge == null) {
				edge = graph.create_edge (parent_node, node);
				edge.safe_set ("headlabel", "1", "");
				edge.safe_set ("arrowhead", "none", "");
				edge.safe_set ("style", "dashed", "");
				edge.safe_set ("label", "e", "");
				edge.safe_set ("fontsize", "8", "");
			}

		}

		Gvc.Node? old_parent_node = parent_node;
		parent_node = node;

		foreach (var sub_element in element.subtree) {
			visit_tree (sub_element);
		}
		parent_node = old_parent_node;
	}
}

int main (string[] args) {
	var language = new Language1 ();
	var source = new Parsing.SourceFile.from_file ("language1.lang");
	var parser = new Parsing.Parser (language, source);
	bool result = parser.parse ();

	if (result == false) {
		parser.print_syntax_errors ();
	}

	Gvc.init ();
	var gvformat = "xdot";
	var gvcontext = new Gvc.Context ();
	var graph_generator = new GraphGenerator ("Test");
	var graph = graph_generator.generate (parser.element);
	gvcontext.layout (graph, "dot");
	gvcontext.render_filename (graph, gvformat, "test." + gvformat);
	return 0;
}
