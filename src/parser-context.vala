/* parser-context.vala
 *
 * Copyright (C) 2011  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 *    Tomaž Vajngerl <quikee@gmail.com>
 */

using Gee;

namespace Parsing {

	public struct ParsePosition {
		public int current_index;
		public int line_character_number;
		public int line_number;

		public ParsePosition () {
			current_index = 0;
			line_character_number = 1;
			line_number = 1;
		}

		public void new_line () {
			line_character_number = 1;
			line_number++;
		}

		public void increment () {
			current_index++;
			line_character_number++;
		}
	}

	public class SyntaxErrorDescription : Object {
		public string element_name { get; private set; }
		public ParsePosition parse_position { get; private set; }
		public string message { get; private set; }

		public SyntaxErrorDescription (string message, ParsePosition parse_position, string element_name = "") {
			this.message = message;
			this.parse_position = parse_position;
			this.element_name = element_name;
		}

		public string to_string () {
			return "Syntax error at %d:%d - %s".printf (parse_position.line_number, parse_position.line_character_number, message);
		}
	}

	protected class ParserContext : Object {
		string? content = null;
		ParsePosition current_parse_position = ParsePosition ();

		public TreeElement element {
			get { return current_element; }
		}

		TreeElement current_element = new TreeElement ();
		Gee.List<TreeElement> stack = new ArrayList<TreeElement> ();
		public Gee.List<SyntaxErrorDescription> syntax_errors = new ArrayList<SyntaxErrorDescription> ();

		public ParserContext (string content) {
			this.content = content;
		}

		public void add_element_content (string content) {
			current_element.content = content;
		}

		public void push () {
			var new_element = new TreeElement ();
			current_element.subtree.add (new_element);
			stack.add (current_element);
			current_element = new_element;
		}

		public void pop () {
			current_element = stack.last ();
			stack.remove_at (stack.size - 1);
		}

		public void remove_last () {
			current_element.subtree.remove_at (current_element.subtree.size - 1);
		}

		public void clear () {
			current_element.subtree.clear ();
		}

		public unichar current () {
			return content[current_parse_position.current_index];
		}

		public void next () {
			if (CharTools.is_new_line (current ())) {
				current_parse_position.new_line ();
			}
			current_parse_position.increment ();
		}

		public bool is_at_end () {
			return current_parse_position.current_index >= content.length;
		}

		public void seal_element_content_interval (ParsePosition begin, ParsePosition end) {
			string parsed_content = content.substring (begin.current_index, end.current_index-begin.current_index);
			add_element_content (parsed_content);
		}

		public ParsePosition get_parse_position () {
			return current_parse_position;
		}

		public void return_to_position (ParsePosition position) {
			current_parse_position = position;
		}

		public void skip_whitespaces () {
			while (CharTools.is_whitespace (current ())) {
				next ();
			}
		}

		public void log_error (string message) {
			syntax_errors.add (new SyntaxErrorDescription (message, current_parse_position));
		}

		public void clear_errors () {
			syntax_errors.clear ();
		}

		public void print_syntax_errors () {
			foreach (SyntaxErrorDescription error in syntax_errors) {
				print ("%s\n", error.to_string ());
			}
		}
	}
}
