/* literal-control.vala
 *
 * Copyright (C) 2011  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 *    Tomaž Vajngerl <quikee@gmail.com>
 */

using Gee;

namespace Parsing {

	public abstract class CombinedLiteral : Literal {

		protected Gee.List<Literal> literal_list = new ArrayList<Literal>();

		protected Gee.List<Literal> get_literal_list() {
			return literal_list;
		}

		public CombinedLiteral(Literal[] literals) {
			foreach (Literal each_literal in literals) {
				literal_list.add (each_literal);
			}
		}

		public void add (Literal literal) {
			literal_list.add (literal);
		}
	}

	public class And : CombinedLiteral {
		public And(Literal[] literals) {
			base(literals);
		}

		public override bool parse (ParserContext context)  {
			foreach (Literal literal in get_literal_list()) {
				context.skip_whitespaces ();
				context.push ();
				bool result = literal.evaluate (context);
				context.pop ();
				if (result == false) {
					return false;
				}
			}
			context.clear_errors ();
			return true;
		}
	}

	public class Or : CombinedLiteral {
		public Or(Literal[] literals) {
			base(literals);
		}

		public override bool parse (ParserContext context)  {
			foreach (Literal literal in get_literal_list()) {
				context.skip_whitespaces ();
				bool result = literal.evaluate (context);
				if (result) {
					context.clear_errors ();
					return true;
				}
			}
			return false;
		}
	}

	public class Forward : Literal {
		public Literal forward_to { get; set; }

		public override bool parse (ParserContext context)  {
			return forward_to.evaluate (context);
		}
	}

	public class OneOrMore : Literal {
		Literal literal;

		public OneOrMore (Literal literal) {
			this.literal = literal;
		}

		public override bool parse (ParserContext context)  {
			ParsePosition position = context.get_parse_position ();
			bool result = false;

			context.push ();
			while (literal.evaluate (context)) {
				result = true;
				position = context.get_parse_position ();
				context.skip_whitespaces ();
				context.pop ();
				context.push ();
			}
			context.pop ();
			context.remove_last ();
			context.return_to_position (position);
			if (result == true) {
				context.clear_errors ();
			} else {
				context.log_error ("OneOrMore");
			}
			return result;
		}
	}

	public class ZeroOrMore : Literal {
		Literal literal;

		public ZeroOrMore (Literal literal) {
			this.literal = literal;
		}

		public override bool parse (ParserContext context)  {
			ParsePosition position = context.get_parse_position ();

			context.push ();
			while (literal.evaluate (context)) {
				position = context.get_parse_position ();
				context.skip_whitespaces ();
				context.pop ();
				context.push ();
			}
			context.pop ();
			context.remove_last ();
			context.return_to_position (position);

			context.clear_errors ();
			return true;
		}
	}

	public class Optional : Literal {
		Literal literal;

		public Optional (Literal literal) {
			this.literal = literal;
		}

		public override bool parse (ParserContext context)  {
			ParsePosition position = context.get_parse_position ();
			var result = literal.evaluate (context);
			if (result == false) {
				context.clear ();
				context.return_to_position (position);
			}
			context.clear_errors ();
			return true;
		}
	}

}
