/* literal-basic.vala
 *
 * Copyright (C) 2011  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 *    Tomaž Vajngerl <quikee@gmail.com>
 */

using Gee;

namespace Parsing {

	public class Base {
		public static EnclosedLiteral STRING () {
			return new EnclosedLiteral ('"');
		}

		public static EnclosedLiteral SSTRING () {
			return new EnclosedLiteral ('\'');
		}

		public static Token semicolon () {
			return new Token (";");
		}

		public static Token colon () {
			return new Token (":");
		}

		public static Token equals () {
			return new Token ("=");
		}
	}

	public class Token : Literal {
		public string match_string { get; private set; }

		public Token (string match_string) {
			this.match_string = match_string;
		}

		public override bool parse (ParserContext context) {
			int index = 0;
			while (!CharTools.is_null (context.current ()) && context.current () == match_string[index] && index < match_string.length) {
				index++;
				context.next ();
			}
			if (index == match_string.length) {
				context.add_element_content (match_string);
				return true;
			} else {
				context.log_error ("TOKEN");
				return false;
			}
		}
	}

	public class Word : AbstractWord {
		public delegate bool Match (int index, unichar character);

		Match match_character_callback;

		protected override bool is_valid_character (int index, unichar character) {
			return match_character_callback(index, character);
		}

		public Word (Match match_character_callback) {
			this.match_character_callback = match_character_callback;
		}
	}

	public abstract class AbstractWord : Literal {
		public abstract bool is_valid_character (int index, unichar character);

		public override bool parse (ParserContext context) {
			ParsePosition begin = context.get_parse_position ();
			int index = 0;

			unichar current_character = context.current ();
			while (!CharTools.is_null (current_character) && !CharTools.is_whitespace (current_character)) {
				if (!is_valid_character (index, current_character)) {
					context.log_error ("WORD");
					return false;
				}
				context.next ();
				current_character = context.current ();
				index++;
			}

			ParsePosition end = context.get_parse_position ();
			context.seal_element_content_interval (begin, end);

			return true;
		}
	}

	public class Alpha : AbstractWord {
		public override bool is_valid_character (int index, unichar character) {
			return character.isalpha ();
		}
	}

	public class AlphaNumberic : AbstractWord {
		public override bool is_valid_character (int index, unichar character) {
			return character.isalnum ();
		}
	}

	public class Numeric : AbstractWord {
		public override bool is_valid_character (int index, unichar character) {
			return character.isdigit ();
		}
	}

	public class ID : Literal {
		public override bool parse (ParserContext context) {
			ParsePosition begin = context.get_parse_position ();
			if (!context.current ().isalpha ()) {
				return false;
			}
			context.next ();
			while (context.current ().isalnum ()) {
				context.next ();
			}
			ParsePosition end = context.get_parse_position ();
			context.seal_element_content_interval (begin, end);
			return true;
		}
	}

	public class EnclosedLiteral : Literal {
		unichar enclosing_character;

		public EnclosedLiteral (unichar enclosing_character) {
			this.enclosing_character = enclosing_character;
		}

		bool is_enclosing_tag (unichar character) {
			return character == enclosing_character;
		}

		bool is_content_character (unichar character) {
			if (is_enclosing_tag (character) || CharTools.is_null (character) || CharTools.is_new_line (character)) {
				return false;
			}
			return true;
		}

		public override bool parse (ParserContext context) {
			if (!is_enclosing_tag (context.current ())) {
				context.log_error ("ENCLOSING");
				return false;
			}

			context.next ();

			ParsePosition begin = context.get_parse_position ();
			ParsePosition end = begin;

			while (is_content_character (context.current ())) {
				context.next ();
				end = context.get_parse_position ();
			}

			bool result = false;
			if (is_enclosing_tag (context.current ())) {
				context.next ();
				context.seal_element_content_interval (begin, end);
				result = true;
			}

			if (result == false) {
				context.log_error ("ENCLOSING");
			}
			return result;
		}
	}
}
