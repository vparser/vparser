using Gee;

namespace Parsing {
	public class TreeElement : Object {
		public Gee.List<TreeElement> subtree = new Gee.ArrayList<TreeElement> ();
		public string? content = null;
		public string? type = null;

		public string to_string () {
			if (subtree.is_empty) {
				return this.content == null ? "''" : "'" + this.content + "'";
			} else {
				var builder = new StringBuilder ();
				builder.append ("[");
				for (int index=0; index < subtree.size; index++) {
					builder.append (subtree.get (index).to_string ());
					if (index != subtree.size - 1) {
						builder.append (", ");
					}
				}
				builder.append ("]");
				return builder.str;
			}
		}
	}

	public delegate void ParserAction (TreeElement current_element);

	public abstract class Literal : Object {
		public ParserAction parser_action { get; set; }

		construct {
			parser_action = null;
		}

		public abstract bool parse (ParserContext context);

		public bool evaluate (ParserContext context) {
			bool result = parse (context);
			if (result == true) {
				TreeElement current_element = context.element;
				current_element.type = this.get_type ().name ();
				if (parser_action != null) {
					parser_action (current_element);
				}
			}
			return result;
		}
	}
}
