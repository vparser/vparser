/* parser.vala
 *
 * Copyright (C) 2011  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 *    Tomaž Vajngerl <quikee@gmail.com>
 */

using Gee;

namespace Parsing {

	public class CharTools {
		public static bool is_null (unichar character) {
			return character == '\0';
		}

		public static bool is_new_line (unichar character) {
			return character == '\n';
		}

		public static bool is_space (unichar character) {
			return character.isspace ();
		}

		public static bool is_tab (unichar character) {
			return character == '\t';
		}

		public static bool is_whitespace (unichar character) {
			return is_space(character) || is_new_line(character) || is_tab(character);
		}
	}

	public class Parser : Object {
		public Literal root { get; private set; }
		public SourceFile? source { get; set; }

		public TreeElement element {
			get { return context != null ? context.element : null; }
		}

		ParserContext context;

		public Parser (Literal root, SourceFile? source = null) {
			this.root = root;
			this.source = source;
		}

		public bool parse_string (string source_string) {
			this.source = new SourceFile.from_string (source_string);
			return parse ();
		}

		public bool parse () {
			if (source == null) {
				return false;
			}

			context = new ParserContext (source.content);
			bool result = root.evaluate (context);
			if (result == false) {
				return false;
			}
			context.skip_whitespaces ();
			if (!context.is_at_end ()) {
				context.log_error ("Not EOF");
				return false;
			}
			return true;
		}

		public void print_syntax_errors () {
			if (context != null) {
				context.print_syntax_errors ();
			}
		}

		public Gee.List<SyntaxErrorDescription> get_list_of_syntax_errors () {
			return context.syntax_errors;
		}

		public string to_string () {
			return element.to_string ();
		}
	}

	public class SourceFile : Object {
		public string content { get; private set; }

		public SourceFile.from_string (string content) {
			this.content = content;
		}

		public SourceFile.from_file (string filename) {
			string content;
			try {
				FileUtils.get_contents (filename, out content);
			} catch (FileError file_error) {
				error (file_error.message);
			}
			this.content = content;
		}
	}
}
